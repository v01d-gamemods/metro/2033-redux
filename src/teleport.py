from trainerbase.common.teleport import Teleport

from objects import player_x, player_y, player_z


tp = Teleport(player_x, player_y, player_z, minimal_movement_vector_length=0.2)
