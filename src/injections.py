from trainerbase.codeinjection import AllocatingCodeInjection, CodeInjection
from trainerbase.memory import pm

from memory import player_coords_address


god_mode = CodeInjection(
    pm.base_address + 0x3C4975,
    """
        or byte [rax + 0x712], 0x40
    """,
)

add_total_ammo_on_spend = CodeInjection(
    pm.base_address + 0x2B2729,
    """
        add [rcx + 0x4F4], ecx
    """,
)

no_reload = CodeInjection(pm.base_address + 0x2C0B2F, "nop\n" * 7)

update_player_coords_address = AllocatingCodeInjection(
    pm.base_address + 0x89E02F,
    f"""
        push rax

        mov rax, {player_coords_address.base_address}
        mov [rax], rdi

        pop rax

        mov [rdi + 0xE8], eax
        mov eax, [r15 + 0xC]
        mov [rdi + 0xEC], eax
    """,
    original_code_length=16,
)
