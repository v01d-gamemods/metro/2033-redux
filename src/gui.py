from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.injections import CodeInjectionUI
from trainerbase.gui.misc import SeparatorUI
from trainerbase.gui.speedhack import SpeedHackUI
from trainerbase.gui.teleport import TeleportUI

from injections import add_total_ammo_on_spend, god_mode, no_reload
from teleport import tp


@simple_trainerbase_menu("Metro 2033 Redux and Last Light Redux", 670, 460)
def run_menu():
    add_components(
        CodeInjectionUI(god_mode, "God Mode", "F1"),
        CodeInjectionUI(no_reload, "No Reload", "F2"),
        CodeInjectionUI(add_total_ammo_on_spend, "Add Total Ammo On Spend", "F3"),
        SeparatorUI(),
        TeleportUI(tp),
        SeparatorUI(),
        SpeedHackUI(),
    )
