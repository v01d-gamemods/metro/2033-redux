from trainerbase.gameobject import GameFloat

from memory import player_coords_address


player_x = GameFloat(player_coords_address + 0x8)
player_y = GameFloat(player_coords_address)
player_z = GameFloat(player_coords_address + 0x4)
