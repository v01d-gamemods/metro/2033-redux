# Metro 2033 Redux & Last Light Redux

2033: *Steam, build 3790582*

Last Light: *Steam, build 3790649*

## How To Run

### Prepare your environment (once)

1. Install [Python](https://www.python.org/downloads/) 3.12

2. Install [Poetry](https://python-poetry.org/)

    ```bash
    python -m pip install --user pipx
    pipx ensurepath
    pipx install poetry
    ```

3. Install [MS C++ Build Tools](https://visualstudio.microsoft.com/visual-cpp-build-tools/)
as they are required to build some dependencies

4. Install dependencies

    ```bash
    poetry install
    ```

### Run

#### Using [Make](https://www.gnu.org/software/make/)

```bash
make
```

#### If Make is not installed

```bash
poetry run python src/main.py
```
